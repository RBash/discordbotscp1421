#!/usr/bin/python
# coding: utf8

import discord
import random

token = 'PLACE_YOUR_OUN_TOKEN_HERE'
#DiceList = ["⚀", "⚁", "⚂", "⚃", "⚄", "⚅"]
DiceList = [":one:", ":two:", ":three:", ":four:", ":five:", ":six:"]
AnswersList = \
    [
        [0.19, "определенно да! "],
        [0.19, "похоже на то. "],
        [0.19, "возможно. "],
        [0.19, "наврятли. "],
        [0.19, "однозначно нет! "],
        [0.05, "ты пидр) "],
    ]
OrthodoxAnswers = \
    [
        [0.19, "100% Orthodox! "],
        [0.19, "Довольно Православно! "],
        [0.19, "Жимолость Может Помочь!"],
        [0.19, "Не Православно!"],
        [0.19, "Вообще Не Православно! "],
        [0.04, "Такую хуйню даже Жимолость не исправит! "],
        [0.01, "Пссс Пацаны Жимолость Не Нужна ?! "],
    ]
FairAnswers = \
    [
        [0.60, "Справедливо! "],
        [0.40, "Не Справедливо! "],
    ]

client = discord.Client()


def weighted_choice(target_list, desired_num=1):
    # chars_to_remove = "[]'"
    answer_list = random.choices([i[1] for i in target_list], [i[0] for i in target_list], k=desired_num)
    answer_string = ''.join(answer_list)
    return answer_string


@client.event
async def on_message(message):
    # we do not want the bot to reply to itself
    if message.author == client.user:
        return

    if message.content.startswith('!предскажи') or message.content.startswith('!Предскажи'):
        G = C = M = i = A = GorC = WHO = 0
        Players = list()
        SCP_LIST = ["SCP-049", "SCP-096", "SCP-106", "SCP-173", "SCP-939-89", "SCP-939-53"]
        Players.clear()
        GorC = random.randint(1, 10)
        if GorC <= 8:
            G = 1
            C = 0
            A = random.randint(1, 8)
            if GorC == A:
                M = 1
        elif GorC > 8:
            C = 1
            G = M = 0
        if M == 1:
            for i in range(12):
                Players.append("Сотрудника класса Д.")
            Players.append("Командир отряда МОГ.")
        elif M == 0:
            for i in range(13):
                Players.append("Сотрудника класса Д.")
        for i in range(3):
            Players.append("Ученого.")
        if G == 1 and C == 0:
            for i in range(5):
                Players.append("Охранника.")
        elif C == 1 and G == 0:
            for i in range(5):
                Players.append("Отряд Хаоса.")
        while len(Players) < 25:
            RAND = random.randint(0, 5)
            if SCP_LIST[RAND] not in Players:
                Players.append(SCP_LIST[RAND])
        random.shuffle(Players)
        msg = '{0.author.mention} тебя заспавнит за {1}'.format(message, random.choice(Players))
        await client.send_message(message.channel, msg)

        if message.content.startswith('!Рулетка') or message.content.startswith('!рулетка'):
            drum = [0, 0, 0, 0, 0, 1]
            shot = random.choice(drum)
            if shot == 1:
                msg = '{0.author.mention} Убит'.format(message)
            else:
                msg = '{0.author.mention} Живой'.format(message)
            await client.send_message(message.channel, msg)

    if message.content.startswith('!Вопрос') or message.content.startswith('!вопрос'):
        if message.content == "!Вопрос" or message.content == "!вопрос":
            msg = '{0.author.mention} Да ты охуел задавать мне пустой вопрос!'.format(message)
            await client.send_message(message.channel, msg)
            return
        msg = '{0.author.mention} мой ответ {1}'.format(message, (weighted_choice(AnswersList, 1)))
        await client.send_message(message.channel, msg)
        
    if message.content.startswith('!Запись') or message.content.startswith('!запись'):
        f = open('text.txt', 'r+')
        if str(message.author) in f:
            msg = '{0.author.mention} Ты уже записался'.format(message)
            await client.send_message(message.channel, msg)
        elif str(message.author) not in f:
            msg = '{0.author.mention} Ты Записался'.format(message)
            f.write(str(message.author) + '\n')
            f.close()
            await client.send_message(message.channel, msg)

    if message.content.startswith('!Бутылка') or message.content.startswith('!бутылка'):
        Fi = open('text.txt', 'r')
        Prin = Fi.readlines()
        msg = ' Сел на Бутылку'.format(message)
        print(Prin)
        await client.send_message(message.channel, str(random.choice(Prin)) + '\n' + msg)
    if message.content.startswith('!Рестарт') or message.content.startswith('!рестарт'):
        msg = '{0.author.mention} '.format(message)
        open('text.txt', 'w').close()
        await client.send_message(message.channel, msg)



    if message.content.startswith('!Кубик') or message.content.startswith('!кубик'):
        msg = '{0.author.mention} {1}'.format(message, random.choice(DiceList))
        await client.send_message(message.channel, msg)

    if message.content.startswith('!Команды') or message.content.startswith('!команды'):
        msg = '{0.author.mention} хуй тебе, а не команды'.format(message)
        await client.send_message(message.channel, msg)

    if message.content.startswith('!Помощь') or message.content.startswith('!помощь'):
        msg = '{0.author.mention} тебе уже ничто не поможет'.format(message)
        await client.send_message(message.channel, msg)

    if message.content.startswith('!Православность') or message.content.startswith('!православность'):
        msg = '{0.author.mention} {1}'.format(message, (weighted_choice(OrthodoxAnswers, 1)))
        await client.send_message(message.channel, msg)

    if message.content.startswith('!Справедливо') or message.content.startswith('!справедливо'):
        msg = '{0.author.mention} {1}'.format(message, (weighted_choice(FairAnswers, 1)))
        await client.send_message(message.channel, msg)

    if message.content.startswith('!Жимолость') or message.content.startswith('!жимолость'):
        msg = '{0.author.mention} ЖИМОЛОСТЬ ЭТО МОЯ ЖИЗНЬ!!!!!1!11!'.format(message)
        await client.send_message(message.channel, msg)


@client.event
async def on_ready():
    print('Logged in as')
    print(client.user.name)
    print(client.user.id)
    print('------')


client.run(token)

